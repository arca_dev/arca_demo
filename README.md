**New Developers start here**

---

## Install DevDockerImage

If you are a new ARCA developer, follow these steps to setup your development machine

1. Install Docker
2. Fetch the **ArcaDockerDevImage** on to your local machine
3. Open the Microsoft Visual Studio Code editor.
3. Start coding.
---
